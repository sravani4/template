const path = require('path');
const alias = require('./webpack/alias');
// const webpack = require('webpack');
const HtmlWebPackPlugin = require("html-webpack-plugin");
module.exports = {

  entry: './src/javascript/index.js',

  resolve: {
    alias: alias,
    modules: [path.resolve(__dirname, "src"), "node_modules"],
    extensions: ['.js']
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              modules: {
              localIdentName: '[name]__[local]___[hash:base64:5]',
              }
            },
          },
        ],
      },

      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
        include: path.join(__dirname, 'src', 'styles')
      },
      {
        test: /\.woff|\.woff2$/,
        use: "url-loader?limit=10000&mimetype=application/font-woff"
      }, {
        test: /\.ttf$/,
        use: "url-loader?limit=10000&mimetype=application/octet-stream"
      }, {
        test: /\.eot|otf$/,
        use: "file-loader"
      }, {
        test: /\.svg$/,
        use: "url-loader?limit=20000&mimetype=image/svg+xml&name=images/[hash].[ext]"
      },{
        test: /\.jpg$/,
        use: "url-loader?limit=20000&mimetype=image/jpg+xml&name=images/[hash].[ext]"
      },
      {
        test: /\.png$/,
        use: "url-loader?mimetype=image/png",
      },
      // {
      //   test: /\.scss$/,
      //   use: ['style-loader', 'css-loader', 'sass-loader'],
      // },
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./index.html",
      filename: "./index.html"
    })
  ]
};
