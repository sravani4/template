
const path = require('path');

module.exports = {
  modules: path.resolve(__dirname, '../src/javascript/redux/modules'),
  config: path.resolve(__dirname, '../src/javascript/config'),
  shapes: path.resolve(__dirname, '../src/javascript/shapes'),
  libs: path.resolve(__dirname, '../src/javascript/libs'),
  componentsDumb: path.resolve(__dirname, '../src/javascript/componentsDumb'),
  componentsSmart: path.resolve(__dirname, '../src/javascript/componentsSmart'),
  components: path.resolve(__dirname, '../src/javascript/components'),
  containers: path.resolve(__dirname, '../src/javascript/containers'),
  constants: path.resolve(__dirname, '../src/javascript/constants'),
  images: path.resolve(__dirname, '../src/images'),
  styles: path.resolve(__dirname, '../src/styles'),
  videos: path.resolve(__dirname, '../src/videos'),
  animations: path.resolve(__dirname, '../src/animations'),
  intls: path.resolve(__dirname, '../src/javascript/intl'),
  'graphql-queries': path.resolve(__dirname, '../src/javascript/graphql'),
  routes: path.resolve(__dirname, '../src/javascript/routes'),
  tracking: path.resolve(__dirname, '../src/javascript/config/tracking'),
  // below given aliases are used for testing.
};
