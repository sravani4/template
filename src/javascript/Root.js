import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';

import { IntlProvider } from 'react-intl';


browserHistory.listen(location => {
  // Use setTimeout to make sure this runs after React Router's own listener
  setTimeout(() => {
    // if you click go back and you enter explore page, scroll the document to (0, 0) coordinates
    if (location.action === 'POP' && location.pathname.includes('explore')) {
      window.scrollTo(0, 0);
      return;
    }
    // Keep default behavior of restoring scroll position when user:
    // - clicked back button
    // - clicked on a link that programmatically calls `history.goBack()`
    // - manually changed the URL in the address bar (here we might want
    // to scroll to top, but we can't differentiate it from the others)
    if (location.action === 'POP') {
      return;
    }
    // In all other cases, check fragment/scroll to top
    const hash = window.location.hash;
    if (hash) {
      const element = document.querySelector(hash);
      if (element) {
        element.scrollIntoView({ block: 'start', behavior: 'smooth' });
      }
    } else window.scrollTo(0, 0);
  });
});


export default class Root extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    routes: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.renderApp = this.renderApp.bind(this);
  }

  renderApp() {
    const { store, routes } = this.props;
    console.log('store and routes', routes(store));
    return (
      <Router
        history={browserHistory}
      >
        {routes(store)}
      </Router>
    );
  }

  render() {
    const { store } = this.props;
    // Global variable to stop double call to onEnter hooks
    window.__appInitialized = true;

  // Global variable which determines whether user follows an external shared affiliate Link
  // or an internal(in App) affiliate link. This is to be set only when the app is initialized
    window.__externalReferral = false;

    const { location: { search = '', pathname = '' } } = window;
    if (search && search.includes('ref')) window.__externalReferral = true;

    // show the mobile page only if it's not a responsive route
    // const shouldRenderMobilePage = isMobile() && !isResponsiveRoute;

    const app = this.renderApp();

    return (
      <div>
        <Provider store={store}>
            {app}
        </Provider>
      </div>
    );
  }

}
