import React, { Component } from "react";
import 'babel-polyfill';
import ReactDOM from "react-dom";
import configureStore from "./redux/store";
import Root from './Root';
import routes from './routes';
import classes from './index.css';


const store = configureStore();

const render = (Component) => {
  ReactDOM.render(
    <div className={classes.container}>
    <Component
      routes={routes}
      store={store}
    />
    </div>,
    document.getElementById('root')
  )
};
render(Root);

