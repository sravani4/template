import { createStore, compose, applyMiddleware } from "redux";

import states from 'modules';
import middleware from '../redux/middleware';



function configureStore() {
 const store = createStore(states, compose(applyMiddleware(middleware),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));
 return store;
}

export default configureStore;
