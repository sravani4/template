export const FETCH_LANDING_FEED_PENDING = 'landingpage/FETCH_LANDING_FEED_PENDING';
export const FETCH_LANDING_FEED_SUCCESS = 'landingpage/FETCH_LANDING_FEED_SUCCESS';
export const FETCH_LANDING_FEED_FAILURE = 'landingpage/FETCH_LANDING_FEED_FAILURE';

export const initialState = {
  loading: true,
  status: false
};

export default function reducer(state = initialState, action = {}) {
  const { type, payload } = action;
  console.log('type', type);
  switch (type) {
  case FETCH_LANDING_FEED_PENDING:
    return {
      loading: true,
      payload
    };
  case FETCH_LANDING_FEED_SUCCESS:
    return {
      ...state,
      loading: false,
      payload
    };
  case FETCH_LANDING_FEED_FAILURE:
    return {
      ...state,
      loading: false
    };

  default:
      return state;
  }
}

export function fetch() {

  const actionParams = {
    type:
      FETCH_LANDING_FEED_PENDING,
      FETCH_LANDING_FEED_SUCCESS,
      FETCH_LANDING_FEED_FAILURE,
    payload: {
      status: true
    }
  };
  return actionParams;
}
