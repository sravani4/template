import * as landingPageActions from 'modules/landingPage';

let __SERVER__ = false;
export default function bindHooks(store) {
  const { getState, dispatch } = store;

  return {

    wrapper: (hook) => (
      __SERVER__ || (!__SERVER__ && (window && !window.__appInitialized))
      ? p => p
      : hook
    ),

    showLandingPage: ({}, transition, next) => {
      console.log('hook call');

      dispatch(landingPageActions.fetch());
      return next();
    },
  }
}
