import React, { Component } from "react";

import { connect } from 'react-redux';

import { fetch } from 'modules/landingPage';
import classes from './index.css';


class Intro extends Component{

  static fetchData(params, query, store, promise) {
    parallel([
      fetch.bind({}, this, store.dispatch)
    ], (err) => err ? promise.reject() : promise.resolve());
  }

  render () {

    return (
      <div className={classes.container}>
       Landing Page
      </div>
    );
  }
}

const mapStateToProps = ({ landingPage }) => {
  return { landingPage };
};


const mapDispatchToProps = dispatch => ({
  fetch:() => dispatch(fetch)
});

export default connect (mapStateToProps, mapDispatchToProps)(Intro);
