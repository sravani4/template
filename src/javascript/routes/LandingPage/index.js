export default (hooks) => ({
  path: '/',

  getComponent(nextState, cb) {
    console.log('route call');
    require.ensure([], (require) => {
      cb(null, require('./components').default);
    });
  },

  onEnter: (() => {
    console.log('hooks value', hooks);
    return hooks.wrapper(hooks.showLandingPage);
  })(),
});
