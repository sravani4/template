import bindHooks from './hooks';
import landingPage from './LandingPage';

export default (store) => {
  console.log('store', store);

  const hooks = bindHooks(store);
  const LandingPage = landingPage(hooks);
  return {

    childRoutes: [
      LandingPage
    ]
  }
}
